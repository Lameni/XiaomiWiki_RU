[Главная страница](../)

## О кастомных прошивках

### [Xiaomi.eu](https://xiaomi.eu/)

Это обычная MIUI прошивка, которую предпочитают большинство людей. Это неофициальная прошивка, основанная на официальной китайской MIUI, с более широкой языковой поддержкой (большинство европейских и некоторых азиатских языков), предустановленными сервисами Google и удалёнными большинством вредоносных программ. Обновления синхронизированы с китайскими сборками. Преимущества по сравнению офицальной глобальной прошивки:

* Xiaomi.eu основан на Chine Stable (Стабильной китайской) / Еженедельных сборках (Weekly builds) (кроме POCOF1, HMNote6Pro, POCOX3P)
* Включён поисковой жест (свайп вверх на рабочем столе)
* 3way меню перезагрузки (в настройках разработчика)
* Удалена реклама в системных приложениях
* Разблокировка по лицу на всех устройствах c MIUI 7.x/8.x/9.x/10.x/11.x/12.x/12.5.x на борту
* Вертикальный виджет часов на блокировке экрана
* Счётчик шагов на экране помощника
* Дополнительные ярлыки на левом экране блокировки
* Жесты пробуждения на устройствах с MIUI 7.x/8.x/9.x/10.x/11.x/12.x/12.5.x на борту
* Возможность предварительной загрузки AI (ИИ) в лаборатории MIUI (MIUI Lab)
* Полноэкранные жесты на устройствах с MIUI 8.x/9.x/10.x/11.x/12.x/12.5.x
* Возможность автоматического расширения уведомления
* Настройка приоритета уведомлений
* Больше опций для редактирования в приложении Mi Галерея
* Включён MiDrive в Mi Проводнике
* Добавлен ландшафтный режим в Сообщениях
* Восход и закат в приложении Погода (Mi Погода)
* Интегрирована поддержка Google приложений
* Импортирование тем с сайта zhuti.xiaomi.com с помощью ThemeManager
* Нет бесполезных китайских приложений-вирусов
* Больше свободной оперативной памяти за счет меньшего количества фоновых процессов
* Унифицированные значки приложений в плоском стиле как для системных, так и для сторонних приложений (например, плоский значок Play Store, в отличие от официального глобального MIUI)
* Расширенное меню с цветными значками (а не просто текст, как в официальных выпусках MIUI)
* Никаких китайских иероглифов в системе
* Mi Видео, Mi Музыка, Mi Браузер: Никакого бесполезного китайского контента
* Нету возможности случайно повторно заблокировать загрузчик с помощью прошивки любого выпуска xiaomi.eu
* Добавлены 27 реально переведённых языков от MIUI фанатов и MIUI фансайтов
* Добавлена поддержка в приложении Телефон Европейский Т9
* Добавлен переключатель звуков зарядки
* Добавлен переключатель проверки орфографии
* Добавлена анимация выключения экрана
* Добавлена ​​предварительная загрузка AI для устройств с MIUI 9.x - 12.5.x
* Оптимизированные настройки GPS для Европы
* Оптимизированно потребление батареи
* Оптимизированно потребление оперативной памяти
* Добавлена ​​поддержка cъёмки 4K видео для MI4C, MI4S, HMNote3Pro, HMNote3SE, HMNote4X
* Добавлен 3D Touch в версию для MI5S с 3 ГБ оперативной памяти.
* Добавлен GSMalphabet в приложение для SMS
* Добавлен дополнительный макет сетки иконок 4x5, 4x6, 4x7, 5x5, 5x6, 5x7 (Зависит от устройства)
* Прохождение SafetyNet (Google Pay)
* Cертификация в Play Маркете
* Деодексирована
* Добавлены автоматизированные задачи
* Добавлен ускоритель скорости игры
* Добавлен модуль стирания (волшебное устранение) в параметры редактирования фотографий приложения Галерея.
* Исправлен низкий уровень громкости через наушники
* Исправлена ​​проблема с голосовым ответом помощника Google.

[Скачать](https://xiaomi.eu/community/link-forums/rom-downloads.73/)

### [LineageOS](https://www.lineageos.org/)

LineageOS - это одна из самых лёгких и популярных кастомных прошивок, основанная на AOSP. Также LineageOS  офицальный премник прошивки CyanogenMod, поддержка которой была прекращена ещё в конце 2016 года.

**Официально поддерживаемые устройства:**

* Mi 3/4 (cancro)
* Mi 5 (gemini)
* Mi 5s (capricorn)
* Mi 5s Plus (natrium)
* Mi 6 (sagit)
* Mi 6X (wayne)
* Mi 8 (dipper)
* Mi 8 Lite (platina)
* Mi 9T / Redmi K20 (davinci)
* Mi 10T / Mi 10T Pro / Redmi K30S Ultra (apollon)
* Mi A1 (tissot)
* Mi MIX (lithium)
* Mi MIX 2 (chiron)
* Mi MIX 2S (polaris)
* Mi Note 2 (scorpio)
* Mi Note 3 (jason)
* Mi A1 (tissot)
* Mi A2 (jasmine_sprout)
* Redmi 2 (wt88047)
* Redmi 3 (ido)
* Redmi 7 (onclite)
* Redmi 3S/3X (land)
* Redmi 4X (santoni)
* Redmi Note 4 (mido)
* Redmi Note 5 / Redmi Note 5 Pro (whyred)
* Redmi Note 6 Pro (twolip)
* Redmi Note 7 (lavender)
* Redmi Note 8 / 8T (ginkgo)
* Redmi Note 9 Pro / Poco M2 Pro / Redmi Note 9S / Redmi Note 9 Pro Max (miatoll)
* POCO F1 (beryllium)
* POCO F2 PRO / Redmi K30 Pro / Redmi K30 Pro Zoom Edition (lmi)
* POCO F3 (alioth)
* POCO X3 NFC (surya)
* POCO X3 Pro (vayu)

[Скачать](https://download.lineageos.org/)

### [Pixel Experience](https://download.pixelexperience.org/)

Pixel Experience - это кастомная прошивка на основе AOSP, которая поставляется с предустановленными приложениями Google и эксклюзивными элементами Pixel. Он нацелен на то, чтобы обеспечить работу, подобную устройству Google Pixel.

**Официально поддерживаемые телефоны:**

* Mi 5 (gemini)
* Mi 5X (tiffany)
* Mi 5s (capricorn)
* Mi 5s Plus (natrium)
* Mi 6X (wayne)
* Mi 8 (dipper)
* Mi 8 Lite (platina)
* Mi 8 SE (sirius)
* Mi 9 (cepheus)
* Mi 9 SE (grus)
* Mi 9T / Redmi K20 (davinci)
* Mi 9T Pro / Redmi K20 Pro (raphael)
* Mi 10i / Mi 10T Lite / Redmi Note 9 Pro 5G (gauguin)
* Mi A1 (tissot)
* Mi A2 (jasmine)
* Mi A2 Lite (daisy)
* Mi A3 (laurel_sprout)
* Mi CC9 Pro / Note 10 (tucana)
* Mi MIX (lithium)
* Mi MIX 2 (chiron)
* Mi MIX 2s (polaris)
* Mi Mix 3 5G (andromeda)
* Mi Note 2 (scorpio)
* POCO F1 (beryllium)
* POCO F2 Pro (lmi)
* POCO X2 / Redmi K30 (phoenix)
* POCO X3/X3 NFC (surya)
* POCO X3 Pro (vayu)
* Redmi 3S/3X (land)
* Redmi 4X (santoni)
* Redmi 5 (rosy)
* Redmi 5A (riva)
* Redmi 6 Pro (sakura)
* Redmi 7A (pine)
* Redmi Note 3 (kenzo)
* Redmi Note 4/4X (mido)
* Redmi Note 5 Global/Pro(whyred)
* Redmi Note 5/Plus (vince)
* Redmi Note 7 (lavender)
* Redmi Note 7 Pro (violet)
* Redmi Note 8 Pro (begonia)
* Redmi Note 8/8T (ginkgo)
* Redmi Note 10 (mojito)
* Redmi Note 10 Pro (sweet)
* Redmi K30 5G / Redmi K30i (picasso)

[Скачать](https://download.pixelexperience.org/)

### [Resurrection Remix](http://www.resurrectionremix.com/)

RR - это форк LineageOS, обспечивающий не только тонну кастомизации и персонализации, но и также нацелен на стабильность и безопасность.

**Официально поддерживаемые устройства:**

* Mi 6X (wayne)
* Mi 8 SE (sirius)
* Mi 9T / Redmi K20 (davinci)
* Mi 9T Pro / Redmi K20 Pro (raphael)
* Mi A1 (tissot)
* Mi A2 (jasmine_sprout)
* POCO F1 (beryllium)
* POCO X2 / Redmi K30 (phoenix)
* POCO X3/X3 NFC (surya)
* Redmi 4X (santoni)
* Redmi 5 (rosy)
* Redmi 5A (riva)
* Redmi 6 Pro (sakura)
* Redmi 7 (onclite)
* Redmi Note 3 (kenzo)
* Redmi Note 4/4X (mido)
* Redmi Note 5 Global/Pro(whyred)
* Redmi Note 5/Plus (vince)
* Redmi Note 7 (lavender)
* Redmi Note 7 Pro (violet)
* Redmi Note 8 Pro (begonia)
* Redmi Note 8/8T (ginkgo)
* Redmi K30 5G / Redmi K30i (picasso)

[Скачать](https://sourceforge.net/projects/resurrectionremix-ten/files/)

### [AOSP Extended](https://www.aospextended.com/)

Цель AEX аналогична цели Resurrection Remix: предоставить стандартный опыт с множеством опций настройки.

**Официально поддерживаемые устройства:**

## Android 10:

* Mi 5 (gemini)
* Mi A1 (tissot)
* Mi A2 Lite (daisy)
* POCO F1 (beryllium)
* Redmi 3S (land)
* Redmi 5 (rosy)
* Redmi 5 Plus/Note 5 (vince)
* Redmi Note 5 Pro (whyred)
* Redmi Note 6 Pro (tulip)
* Redmi Note 7 Pro (violet)

## Android 11:

* Mi 5 (gemini)
* POCO F1 (beryllium)
* Redmi 3S (land)
* Redmi Note 5 Pro (whyred)
* Redmi Note 7 Pro (violet)
* Redmi Note 9S/9 Pro/9 Pro Max/POCO M2 Pro (miatoll)


[Скачать](https://downloads.aospextended.com/)
