[Главная страница](../)

## Инструменты для девайсов Xiaomi

### MiUnlock by Xiaomi

**Поддерживаемая платформа:** Windows

**Особенности:**

* Разблокировка загрузчика на любом Xiaomi устройстве

[Скачать](http://en.miui.com/unlock/download_en.html)

### MiFlash by Xiaomi (не работает с разделами /super)

**Поддерживаемая платформа:** Windows

**Особенности:**

* Установка нужных драйверов для прошивки
* Установка прошивок для Fastboot в режиме Fastboot
* Установка прошивок для Fastboot в режиме EDL
* Блокирует загрузчик на любом Xiaomi устройстве

[Скачать (2019.07.01)](https://download.appmifile.com/images/2019/07/01/09cdc3a7-5a11-42aa-81f4-be27fe12ce80.msi)

**Старые версии:**

[Скачать (2018.05.28)](http://bigota.d.miui.com/tools/MiFlash2018-5-28-0.zip)

[Скачать (2017.04.25)](http://api.en.miui.com/url/MiFlashTool)

[Скачать (2016.08.30)](https://drive.google.com/open?id=0B9wtW2KGOf0RV0F1bnVmYmNFZGM)

[Скачать (2016.04.01)](https://drive.google.com/open?id=0B9wtW2KGOf0RaERVZXFtaGxfZ2s)

### MiFlash Pro by Xiaomi (не работает с разделами /super)

**Поддерживаемая платформа:** Windows

**Особенности:**

* Скачивание прошивок для Fastboot и Recovery
* Установка прошивок для Recovery в режиме Recovery
* Установка прошивок для Fastboot в режиме Fastboot
* Прошивка устройства в режиме Recovery

[Скачать (5.3.714.36)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u1106245679/5.3.714.36/miflash_pro-en-5.3.714.36.zip)

**Старые версии:**

[Скачать (4.3.1220.29)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u1106245679/4.3.1220.29/miflash_pro-en-4.3.1220.29.zip)

[Скачать (4.3.1129.28)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u1106245679/4.3.1129.28/miflash_pro-en-4.3.1129.28.zip)

[Скачать (4.3.1108.24)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u1106245679/4.3.1108.24/miflash_pro-en-4.3.1108.24.zip)

[Скачать (4.3.1106.23)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u1106245679/4.3.1106.23/miflash_pro-en-4.3.1106.23.zip)

[Скачать (3.3.1212.88)](https://drive.google.com/file/d/15eSeySg4JPtO6FAbf8q7PSM67tEHb9Ht/view)

[Скачать (3.3.1112.82)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u1106245679/3.3.1112.82/miflash_pro-en-3.3.1112.82.zip)

[Скачать (3.3.703.67)](https://mega.nz/#!zoIizCQJ!ojy5kaeV4JkEYOXyrNGABTPDbEwCWme6YSf8I9bCPuY)

[Скачать (3.3.518.58)](http://xiaomi-miui-ota-3rdrom.ks3-cn-beijing.ksyun.com/rom/u265827351/3.3.518.58/miflash_pro-3.3.518.58.zip)


### XiaomiTool V2 by Francesco Tescari

[Official website](https://www.xiaomitool.com/V2/)

**Поддерживаемая платформа:** Windows/Mac/Linux

**Особенности:**

* Скачивание прошивок для Fastboot и Recovery
* Разблокировка загрузчика на любом Xiaomi устройстве (не всегда работает)
* Автоматическая установка прошивки

[Скачать](https://www.xiaomitool.com/V2/latest)

### XiaomiADB by Francesco Tescari

[Official website](http://www.xiaomitool.com/adb)

**Поддерживаемая платформа:** Windows

**Особенности:**

* Установка офицальных прошивок для Recovery в MiRecovery
* Почти все возможности ADB (Android Debug Bridge)

[Скачать](http://www.xiaomitool.com/latestadb)

### MiUnlockTool by Francesco Tescari (не всегда работает)

[Official thread on XDA](https://forum.xda-developers.com/android/software-hacking/tool-miunlocktool-unlock-bootloader-t3782444)

**Поддерживаемая платформа:** Windows/Mac/Linux

**Особенности:**

* Разблокировка загрузчика на любом Xiaomi устройстве

[Скачать](http://xiaomitool.com/latestmut)

### Xiaomi ADB/Fastboot Tools by Szaki

**Поддерживаемая платформа:** Windows/Mac/Linux

**Особенности:**

* Деблоатинг любого Xiaomi устройства, удаление/отключение/переустанова/включение любого системного приложения, которое вы хотите
* Включение camera2 или EIS (Требуется установленный TWRP)
* Передача файлов между устройством и компьютером
* Получение более подробной информации о любом Xiaomi устройстве
* Изменение плотности экрана (DPI) или разрешения экрана
* Прошивка любого раздела с помощью образа
* Загрузка с помощью образа
* Установка прошивок для Fastboot в режиме Fastboot
* Очистка кэша или сброс до заводских настроек на любом Xiaomi устройстве
* Разблокировка или заблокировка загрузчика на Xiaomi с оболочкой Android One
* Получить ссылки на последние прошивки для Fastboot или сразу же их cкачать
* Переключение между system/fastboot/EDL/recovery режимами

[Скачать](https://szaki.github.io/XiaomiADBFastbootTools/)
