# Xiaomi Wiki на Русском языке

## Кладовая знаний

* [Советы и рекомендации](wiki/Tips_and_tricks.md)
* [Рекомендованные торговые посредники](wiki/Recommended_resellers.md)
* [Регоиональные названия Xiaomi устройств](wiki/Regional_names_of_Xiaomi_devices.md)
* [Регоиональные версии Xiaomi устройств](wiki/Regional_versions_of_Xiaomi_devices.md)
* [Список проводных наушников Xiaomi](wiki/List_of_wired_Xiaomi_earphones.md)
* [Подтвреждение подлиности продукта Xiaomi](wiki/Verify_the_authenticity_of_Xiaomi_products.md)

---

* [О MIUI](wiki/About_MIUI.md)
* [О кастомных прошивках](wiki/About_custom_ROMs.md)
* [О Fastboot и Recovery прошивках](wiki/About_Fastboot_and_Recovery_ROMs.md)
* [О защите от отката (Anti-Rollback Protection или же сокращённо ARP)](wiki/About_Anti-Rollback_Protection.md)

## Инструкции

[Инструменты для девайсов Xiaomi](wiki/Tools_for_Xiaomi_devices.md)

### Основное

* [Установка офицальных прошивок](wiki/Flash_official_ROMs.md)
* [Удаление системных приложений](wiki/Uninstall_system_apps.md)
* [Отключение рекламы на MIUI](wiki/Disable_ads_in_MIUI.md)
* [Устранение проблем с уведомлениями на MIUI](wiki/Fix_notifications_on_MIUI.md)
* [Настройка китайской MIUI для глобального использования](wiki/Tweak_the_China_ROM_for_global_use.md)

### Дополнительное

* [Разблокировка загрузчика](wiki/Unlock_the_bootloader.md)
* [Установка TWRP и кастомных прошивок](wiki/Flash_TWRP_and_custom_ROMs.md)
* [Включение Camera2](wiki/Enable_Camera2.md)
* [Доступ к EDL mode](wiki/Access_EDL_mode.md)
* [Устранение проблем с сенсором](wiki/Fix_sensor_issues.md)
